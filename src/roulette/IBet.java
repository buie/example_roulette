package roulette;

public interface IBet {

    int payout(int wager);

    String toString();

    void place();

    boolean isMade(Wheel.SpinResult spinResult);

}
