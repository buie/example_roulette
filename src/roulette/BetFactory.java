package roulette;

import roulette.bets.OddEven;
import roulette.bets.RedBlack;
import roulette.bets.ThreeConsecutive;

public class BetFactory {

    public IBet getBet(String bet, String description, int odds) {
        if ("OddEven".equalsIgnoreCase(bet)) {
            return new OddEven(description, odds);
        } else if ("RedBlack".equalsIgnoreCase(bet)) {
            return new RedBlack(description, odds);
        } else if ("ThreeConsecutive".equalsIgnoreCase(bet)) {
            return new ThreeConsecutive(description, odds);
        }

        return null;
    }
}
